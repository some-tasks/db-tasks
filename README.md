# Tasks from DB

### Create a public repository (GitHub / GitLab / Bitbucket) with Terraform module which:
- creates a managed instance group of VMs
- each VM is serving a Docker image (choose any) that listens for connections on some port
- creates a load balancer with managed instance group as backend that load balances incoming HTTP calls to it
- simulate 2 different environments (DEV / PROD) with separate configurations (e.g. size of instance group in DEV = 2, PROD = 3).


### Export aws credentials
```sh
export AWS_ACCESS_KEY_ID=some
export AWS_SECRET_ACCESS_KEY=some
```
### Create admin-keys.pem
Copy to assets/admin-keys.pem
and modify rights
```sh
chmod 400 assets/adim-keys.pem
```

### Update variable "my_local_cidr" with
```sh
curl ifconfig.me
```

### Run init
```sh
terraform init
```

### Select workspace
```sh
terraform workspace select dev
```
or
```sh
terraform workspace select prod
```
### Run terraform
```sh
terraform plan -var-file=vars/dev.tfvars # or terraform plan  -var-file=vars/prod.tfvars
terraform apply -var-file=vars/dev.tfvars -auto-approve # or terraform apply  -var-file=vars/prod.tfvars -auto-approve
```

### After completing build, use load balancer dns name from output
```sh
curl <load balancer dns name>
```

### To destroy environment
```sh
terraform destroy -var-file=vars/dev.tfvars -auto-approve # or terraform destroy -var-file=vars/prod.tfvars -auto-approve
```