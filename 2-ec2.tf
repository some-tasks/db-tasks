resource "aws_instance" "ec2" {
  count                       = var.ec2_count
  ami                         = "ami-078a331927399adac"
  instance_type               = "t2.micro"
  key_name                    = "admin-key"
  associate_public_ip_address = true
  security_groups             = [aws_security_group.public.id]
  subnet_id                   = element(aws_subnet.public.*.id, count.index)

  provisioner "remote-exec" {
      inline = [
        "docker run --rm --name nginx -p 80:80 -d nginx",
      ]
      connection {
        type                  = "ssh"
        host                  = self.public_ip
        user                  = "ec2-user"
        private_key           = file("assets/admin-key.pem")
      }
    }
  tags = {
    Name        = "${var.task}_ec2-${var.env}"
  }
}