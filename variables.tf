variable "task" {
  type          = string
}
variable "cidr_block" {
  type          = string
}

variable "subnets" {
  type          = list(string)
}
variable "azs" {
  type          = list(string)
}

variable "my_local_cidr" {
    type        = list
    default     = ["88.208.115.106/32"]
}

variable "env" {
    type = string
}

variable "ec2_count" {
    type = string
}