env             = "prod"
task            = "production"
region          = "us-east-1"
cidr_block      = "10.0.1.0/26"
subnets         = ["10.0.1.0/28", "10.0.1.16/28"]
azs             = ["us-east-1a", "us-east-1c"]
ec2_count       = 2