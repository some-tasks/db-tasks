resource "aws_lb" "main" {
  name               = "main-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.public.id]
  subnets            = aws_subnet.public.*.id

  enable_deletion_protection = false
  

  tags = {
    Name               = "${var.task}_lb-${var.env}"
  }
}

resource "aws_lb_listener" "listener" {  
  load_balancer_arn = aws_lb.main.arn
  port              = "80"
  protocol          = "HTTP"
  
  default_action {    
    target_group_arn = aws_lb_target_group.main.arn
    type             = "forward"  
  }
}

resource "aws_lb_listener_rule" "listener_rule" {
  depends_on   = [aws_lb_target_group.main]  
  listener_arn = aws_lb_listener.listener.arn
  priority     =  10
  action {    
    type             = "forward"    
    target_group_arn = aws_lb_target_group.main.id 
  }
  condition {
    path_pattern {
      values = ["/*"]
    }
  }
}

resource "aws_lb_target_group" "main" {
  name = "nginx-target"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id


  health_check {
    healthy_threshold   = 2
    interval            = 30
    protocol            = "HTTP"
    unhealthy_threshold = 2
  }

  depends_on = [
    aws_lb.main
  ]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_target_group_attachment" "main" {
  count            = length(aws_instance.ec2)
  target_group_arn = aws_lb_target_group.main.arn
  target_id        = aws_instance.ec2[count.index].id
  port             = 80
}


